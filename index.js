const app = require('express')();
const morgan = require('morgan')

app.use(morgan('dev'))

app.get('/', (req, res) => {
  res.send({msg: 'ok'})
})

app.use((req, res, next) => {
  res.status(404).send({err: 'invalid route'})
})

app.listen(process.env.PORT || 3000, () => console.log(`listening ${process.env.PORT || 3000}`))